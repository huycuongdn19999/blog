import React, { useState } from "react";
import { View, Text, StyleSheet, TextInput, Button } from "react-native";

const BlogPostForm = ({ onSubmit, initialValues }) => {
  const [title, setTitle] = useState(initialValues.title);
  const [content, setContent] = useState(initialValues.content);
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Enter Title:</Text>
      <TextInput
        style={styles.textInput}
        value={title}
        onChangeText={(text) => {
          setTitle(text);
          console.log(text);
        }}
      ></TextInput>
      <Text style={styles.title}>Enter Content:</Text>
      <TextInput
        style={styles.textInput}
        value={content}
        onChangeText={(text) => setContent(text)}
      ></TextInput>
      <Button
        title="Save"
        onPress={() => {
          onSubmit(title, content);
        }}
      ></Button>
    </View>
  );
};

BlogPostForm.defaultProps = {
  initialValues: {
    title: "",
    content: "",
  },
};
const styles = StyleSheet.create({
  container: {
    marginHorizontal: 20,
  },
  title: {
    fontSize: 20,
    paddingBottom: 20,
  },
  textInput: {
    paddingBottom: 10,
    borderColor: "gray",
    borderWidth: 1,
    fontSize: 18,
  },
});
export default BlogPostForm;
