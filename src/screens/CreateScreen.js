import React, { useContext } from "react";
import { StyleSheet } from "react-native";
import BlogPostForm from "../components/BlogPostForm";
import { Context } from "../context/BlogContext";

const CreateScreen = ({ route, navigation }) => {
  const { addBlogPost } = useContext(Context);
  return (
    <BlogPostForm
      onSubmit={(title, content) => {
        addBlogPost(title, content, () => navigation.navigate("Index"));
      }}
    ></BlogPostForm>
  );
  //   return (
  //     <View style={styles.container}>
  //       <Text style={styles.title}>Enter Title:</Text>
  //       <TextInput
  //         style={styles.textInput}
  //         value={title}
  //         onChangeText={(text) => {
  //           setTitle(text);
  //           console.log(text);
  //         }}
  //       ></TextInput>
  //       <Text style={styles.title}>Enter Content:</Text>
  //       <TextInput
  //         style={styles.textInput}
  //         value={content}
  //         onChangeText={(text) => setContent(text)}
  //       ></TextInput>
  //       <Button
  //         title="Save"
  //         onPress={() => {
  //           addBlogPost(title, content, () => {
  //             navigation.navigate("Index");
  //           });
  //         }}
  //       ></Button>
  //     </View>
  //   );
};
const styles = StyleSheet.create({});
export default CreateScreen;
