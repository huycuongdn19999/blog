import React, { useContext } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { AntDesign } from "@expo/vector-icons";
import { Context } from "../context/BlogContext";

const ShowScreen = ({ route, navigation }) => {
  const { id } = route.params;
  navigation.setOptions({
    headerRight: () => (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate("Edit", { id });
        }}
      >
        <AntDesign name="edit" size={24} color="black" />
      </TouchableOpacity>
    ),
  });
  const { state } = useContext(Context);
  const blogPost = state.find((blogPost) => blogPost.id === id);
  console.log(blogPost.title);
  return (
    <View>
      <Text>{blogPost.title}</Text>
      <Text>{blogPost.content}</Text>
    </View>
  );
};
const styles = StyleSheet.create({});
export default ShowScreen;
