import React from "react";
import {
  NavigationContainer,
  NavigationContext,
} from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import IndexScreen from "./src/screens/IndexScreen";
import ShowScreen from "./src/screens/ShowScreen";
import CreateScreen from "./src/screens/CreateScreen";
import EditScreen from "./src/screens/EditScreen";
import { Provider } from "./src/context/BlogContext";

const Stack = createStackNavigator();
function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Index" component={IndexScreen}></Stack.Screen>
        <Stack.Screen name="Show Detail" component={ShowScreen}></Stack.Screen>
        <Stack.Screen name="Edit" component={EditScreen}></Stack.Screen>
        <Stack.Screen
          name="Create Blog"
          component={CreateScreen}
        ></Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default () => {
  return (
    <Provider>
      <App></App>
    </Provider>
  );
};
